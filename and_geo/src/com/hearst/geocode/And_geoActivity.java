package com.hearst.geocode;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.xml.sax.InputSource;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class And_geoActivity extends Activity implements OnClickListener{
    
	TextView txt;
	TextView txt2;
	double latitude;
	double longitude;
	String zip;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(this);
        txt = (TextView)findViewById(R.id.txt);
        txt2 = (TextView)findViewById(R.id.txt2);
    }
    
    private void showMyAddress(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Geocoder myLocation = new Geocoder(getApplicationContext(), Locale.getDefault());   
        List<Address> myList;
        try {
            myList = myLocation.getFromLocation(latitude, longitude, 1);
            if(myList.size() == 1) {
                txt.setText(myList.get(0).toString()); 
                //int i = zip.indexOf("postalCode");
                zip = myList.get(0).toString();
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE); 
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if(location != null) {
            showMyAddress(location);
        }

        final LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                showMyAddress(location);
            }

            public void onProviderDisabled(String arg0) {
                // TODO Auto-generated method stub

            }

            public void onProviderEnabled(String arg0) {
                // TODO Auto-generated method stub

            }

            public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
                // TODO Auto-generated method stub

            }
        };
        
        InputSource inputSource = new InputSource("https://maps.googleapis.com/maps/api/geocode/xml?latlng="+latitude+","+longitude+"&sensor=true");
	}
	//I am adding a test comment here, so that i can add this file again to the git repository for testing.
}